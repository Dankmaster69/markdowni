﻿# Inhaltsverzeichnis Arbeitsauftrag Webcam für die Lehrperson

######  Autoren Sebastian Leibundgut& Sandro Filisetti
###### Version 1.0
---
## Funktion unseres Raspis
Unser service soll eine einen einfachen Einstieg in das Raspi Modul ermöglichen indem eine Gut dokumentiere und leicht zu verstehende Aufgabenstellung dargegeben wird.
Wir werden Fragen bezüglich des Modules einbinden und um die Schritte für das einrichten, bewältigen zu können müssen immer wieder kleinere Aufgaben gelöst werden.


## Was für eine Hard/Software wird benötigt?
### Hardware 
Kamera mit einem USB- Anschluss welcher kompatibel mit dem Raspi ist.

### Software
VNC Viewer -> Auf Raspi und auf dem eigenen Gerät installieren.
VNC Server Server wird nur auf dem eigenen Gerät benötigt.
AUf dem Raspi  wird das Paket fswebcam benötigt.  (Sudo apt-get fswebcam)
Aktuellstes OS und Firmware für den Raspi, hier gibt es keine Limite.

## Installationsanleitung
Die Schüler müssen folgende Schritte bewältigen damit eine benutzung der Webcam gewährleistet wird.

Das Paket: fswebcam installieren, diese wird mit dem folgenden Befehl gemacht.

	"suda apt-get install fswebcam"

2. Der Benurzer muss der Video Gruppe hinzugefügt werden:

	"sudo usermod -a -G Video pi"

(pi ist der username und Video die Gruppe) 

4. Testen ob es ein Bild aufnehmen kann:

		"fswebcam image.jpg"
	
(Dadurch wird ein jpg aufgenommen und mit dem Namen image.jpg gespeichert)

3.1 Bei korrekter ausführung sollte folgender Text abgebildet werden:

![](images/Bild4.png)

Wollen wir nun einschalten, dass es jede Minute automatisch ein Bild aufgenommen wird. Kann dies mit einem Bash Skript ausgeführt werden, dazu wird aber noch ein Verzeichniss benötigt. Vorzugsweise mit "Bilder" oder "Webcam" das Verzeichnis bennen.

Cmds= mkdir webcam ( für den Ordner)
Cmds= sudo nano (zum erstellen des Skriptes)

![](images/Bild7.png)

Zu beachten ist: -r steht für die Auflösung im Bild wird angezeigt  das unser Bild zb 1280x720 ist.

Die Datei sollten mit Namen wie z.B. webcam.sh gespeichert werden.

Die Fotos werden automatisch mit dem Namen des jeweiligen Datums gespeichert, 

Zum ausführen des Skriptes muss nur noch ./webcam.sh 
in das Terminal eingegeben werden.


Für die Automatisierung müssen folgende Schritte befolgt werden.

	"contrab -e"

Ruft ein Menü ab bei welchem man auswählen kann, welcher Editor verwendet wird um das Konfigurations File zu bearbeiten

Wir verwenden Nano, da dies unser Präferierter Editor ist.

Wir müssen dem File sagen, was es ausführen soll.
Das wird mit der Folgenden Eingabe getätig.

    ***** /home/pi/webcam.sh 2>&1


---

Wenn die Gesamte Anleitung durchgeführt wurde ohne Fehler, dann sollte jetzt eine Verbindung auf den Raspi mit VNC möglich sein und die Einstellung der Kamera sollte erledigt sein, sodass diese Minütlich ein Bild generiert wird.
